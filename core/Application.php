<?php

namespace uks;
use uks\contracts\ContainerAbstract;
use uks\contracts\RunnableInterface;
use uks\contracts\ContainerInterface;

/* класс Application, расширяет абстрактный класс ContainerAbstract и реализует интерфейсы RunnableInterface и ContainerInterface. */
class Application extends ContainerAbstract implements RunnableInterface, ContainerInterface 
{
			
	private static $instace = null;

	private function __construct()
	{
        
	}
	private function __clone()
	{

	}
	
	public function setConfig()
	{

	}
	
	public static function getIntstance() 
	{
		if (is_null(self::$instace)){
			self::$instace = new self();		    
		}

		return new self();
	}

	public function run()
	{
		/*if (!empty) {

		}*/
		echo 'Hello';
	}

	
	public function bootstrap()
	{
		
	}

	public function get()
	{
		
	}
	
	public function has()
	{
		
	}


	
}


	
	/*Добавить статическое свойство instance и описать статический метод getIntstance*/
